#!/bin/sh -e

mkdir -p /data/modules /data/npm-cache
npm config set cache=/data/npm-cache

if [[ -n "$MM_CONFIG_URL" ]]; then
    curl -sfo /config/config.js "$MM_CONFIG_URL" || true
fi

if [[ -n "$MM_CSS_URL" ]]; then
    curl -sfo /config/custom.css "$MM_CSS_URL" || true
fi

ln -sf /config/config.js /opt/magicmirror/config/
ln -sf /config/custom.css /opt/magicmirror/css/

node -e "console.log(JSON.stringify(require('/config/config.js').modules));" | jq -r ".[] | .module, .repository, .version, .disable" | while read module; do
    read repo
    read version
    read disabled

    if [[ "$repo" == "null" ]] || [[ "$disabled" == "true" ]]; then
        continue
    fi

    if [[ ! -d "/data/modules/$module" ]]; then
        git clone --bare -- "$repo" "/data/modules/$module"
        cd "/data/modules/$module"
    else
        cd "/data/modules/$module"
        git remote set-url origin "$repo"
        git fetch --force origin '*:*'
    fi

    if [[ "$version" == "null" ]]; then
        version="$( git rev-parse --abbrev-ref HEAD )"
    fi

    if [[ ! -d "/opt/magicmirror/modules/$module" ]]; then
        git worktree prune
        git worktree add --detach "/opt/magicmirror/modules/$module" "$version"
    fi

    cd "/opt/magicmirror/modules/$module"
    if [[ -f "package.json" ]]; then
        npm install
    fi
done

cd /opt/magicmirror
exec "$@"
