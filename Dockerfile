FROM alpine:3.20

ARG MAGICMIRROR_VERSION
ARG MAGICMIRROR_REPO

EXPOSE 8080

ENTRYPOINT ["/sbin/init.sh"]

CMD ["node", "serveronly"]

VOLUME ["/config", "/data"]

HEALTHCHECK CMD curl --fail http://localhost:8080/ || exit 1

COPY init.sh /sbin/init.sh

ENV \
  MAGICMIRROR_VERSION=${MAGICMIRROR_VERSION:-v2.29.0} \
  MAGICMIRROR_REPO=${MAGICMIRROR_REPO:-https://github.com/MagicMirrorOrg/MagicMirror.git}

RUN set -ex; \
    apk --update --no-cache add curl git jq nodejs npm tzdata; \
    git config --global advice.detachedHead false; \
    git clone --branch $MAGICMIRROR_VERSION --depth 1 -- $MAGICMIRROR_REPO /opt/magicmirror; \
    cd /opt/magicmirror; \
    npm run install-mm
